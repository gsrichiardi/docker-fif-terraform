FROM alpine:latest as intermediate
MAINTAINER "DevOps FIF Team <devops.fif@falabella.cl>"

ENV TERRAFORM_VERSION=0.11.13
ENV TERRAFORM_SHA256SUM=5925cd4d81e7d8f42a0054df2aafd66e2ab7408dbed2bd748f0022cfe592f8d2

RUN apk add --update --no-cache curl
RUN curl -s https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip > terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    echo "${TERRAFORM_SHA256SUM}  terraform_${TERRAFORM_VERSION}_linux_amd64.zip" > terraform_${TERRAFORM_VERSION}_SHA256SUMS && \
    sha256sum -cs terraform_${TERRAFORM_VERSION}_SHA256SUMS && \
    unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /bin && \
    rm -f terraform_${TERRAFORM_VERSION}_linux_amd64.zip

ENV VAULT_VERSION=1.0.1
ENV VAULT_SHA256SUM=1ea02627d0157dc844012995123f1cde86a02cb3b45030a9526057d12ee8d924

RUN curl -s https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip > vault_${VAULT_VERSION}_linux_amd64.zip && \
echo "${VAULT_SHA256SUM}  vault_${VAULT_VERSION}_linux_amd64.zip" > vault_${VAULT_VERSION}_SHA256SUMS && \
sha256sum -cs vault_${VAULT_VERSION}_SHA256SUMS && \
unzip vault_${VAULT_VERSION}_linux_amd64.zip -d /bin && \
rm -f vault_${VAULT_VERSION}_linux_amd64.zip

ENV ENVCONSUL_VERSION=0.7.3
ENV ENVCONSUL_SHA256SUM=67ed44cb254da24ca5156ada6d04e3cfeba248ca0c50a5ddd42282cbafde80bc
RUN curl -s https://releases.hashicorp.com/envconsul/${ENVCONSUL_VERSION}/envconsul_${ENVCONSUL_VERSION}_linux_amd64.zip > envconsul_${ENVCONSUL_VERSION}_linux_amd64.zip && \
    echo "${ENVCONSUL_SHA256SUM}  envconsul_${ENVCONSUL_VERSION}_linux_amd64.zip" > envconsul_${ENVCONSUL_VERSION}_SHA256SUMS && \
    sha256sum -cs envconsul_${ENVCONSUL_VERSION}_SHA256SUMS && \
    unzip envconsul_${ENVCONSUL_VERSION}_linux_amd64.zip -d /bin && \
    rm -f envconsul_${ENVCONSUL_VERSION}_linux_amd64.zip

ENV KONG_PLUGIN_VERSION=1.9.1
ENV KONG_PLUGIN_SHA256SUM=61f888d9c9027f8ad0e41bef997c851943afc21dc1f739ec05016eb404248261
RUN curl -o terraform-provider-kong_${KONG_PLUGIN_VERSION}_linux_amd64.zip -sfSL https://github.com/kevholditch/terraform-provider-kong/releases/download/v${KONG_PLUGIN_VERSION}/terraform-provider-kong_${KONG_PLUGIN_VERSION}_linux_amd64.zip
RUN echo "${KONG_PLUGIN_SHA256SUM}  terraform-provider-kong_${KONG_PLUGIN_VERSION}_linux_amd64.zip" > terraform-provider-kong_${KONG_PLUGIN_VERSION}_SHA256SUMS
RUN sha256sum -cs terraform-provider-kong_${KONG_PLUGIN_VERSION}_SHA256SUMS
RUN mkdir -p $HOME/.terraform.d/plugins/linux_amd64
RUN unzip terraform-provider-kong_${KONG_PLUGIN_VERSION}_linux_amd64.zip -d $HOME/.terraform.d/plugins/linux_amd64/
RUN rm -f terraform-provider-kong_${KONG_PLUGIN_VERSION}_linux_amd64.zip


# Solo terraform 216MB --> 195MB
FROM alpine:latest

RUN apk add --update --no-cache git openssh ca-certificates postgresql-client curl jq
RUN mkdir -p $HOME/.terraform.d/plugins/linux_amd64/
ENV KONG_PLUGIN_VERSION=1.9.1

COPY --from=intermediate /bin/terraform /bin/terraform
COPY --from=intermediate /bin/vault /bin/vault
COPY --from=intermediate /bin/envconsul /bin/envconsul
COPY --from=intermediate /root/.terraform.d/plugins/linux_amd64/terraform-provider-kong_v${KONG_PLUGIN_VERSION} /root/.terraform.d/plugins/linux_amd64/terraform-provider-kong_v${KONG_PLUGIN_VERSION}

# TODO: Agregar hardening
# COPY hardening.sh .
# RUN ./hardening.sh

ENTRYPOINT ["/bin/terraform"]
