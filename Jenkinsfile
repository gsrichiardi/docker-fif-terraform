#!groovy
try {
  node('azure-slave') {
    stage('Clean Workspace'){
      sh("find ${WORKSPACE} -mindepth 1 -delete")
    }

    stage('Checkout code') {
      def scmvars = checkout([
        $class: 'GitSCM',
        branches: scm.branches,
        doGenerateSubmoduleConfigurations: scm.doGenerateSubmoduleConfigurations,
        extensions: [[$class: 'CloneOption', noTags: false, shallow: false, depth: 0, reference: '']],
        userRemoteConfigs: scm.userRemoteConfigs,
      ])
      env.GIT_BRANCH = scmvars.GIT_BRANCH
      env.GIT_COMMIT = scmvars.GIT_COMMIT
      env.GIT_PREVIOUS_COMMIT = scmvars.GIT_PREVIOUS_COMMIT
      env.GIT_PREVIOUS_SUCCESSFUL_COMMIT = scmvars.GIT_PREVIOUS_SUCCESSFUL_COMMIT
      env.GIT_URL = scmvars.GIT_URL
      bitbucketStatusNotify ( buildState: 'INPROGRESS' )
    }

    stage('Building Docker Image') {
      def dirpath="."
      def terraformversion = sh(returnStdout: true, script: "grep 'ENV TERRAFORM_VERSION' ${dirpath}/Dockerfile | cut -d= -f2").trim()
      def release = env.GIT_BRANCH.replace('origin/', '')
      if(env.GIT_BRANCH == 'origin/master'){
        try {
          release = sh(returnStdout: true, script: "variable=\$(git describe --tags --abbrev=0 --match '${terraformversion}-*' | cut -d- -f2 | egrep -e '[0-9]+' | sort -r | head -n1); echo \$((variable+1))").trim()
        } catch(error) {
          release = '1'
        }
      }

      env.DOCKERTAG = "${terraformversion}-${release}"
      if(env.GIT_PREVIOUS_SUCCESSFUL_COMMIT == 'null'){
        env.REBUILD = 'true'
      } else {
        env.REBUILD = sh(script: "git diff --name-only ${env.GIT_PREVIOUS_SUCCESSFUL_COMMIT}..${env.GIT_COMMIT} | grep -e Dockerfile", returnStatus: true) == 0
      }

      if(env.REBUILD == 'true'){
        sh """
          docker build --rm=true -t 'fif-terraform:${DOCKERTAG}' ${dirpath}/.
        """
      } else {
        print "Skip step: env.REBUILD == '$REBUILD'"
      }
    }

    stage('Anchore Analysis') {
      if(env.REBUILD == 'true'){
        sh '''
          docker pull anchore/jenkins
          echo "fif-terraform:${DOCKERTAG}" > images.txt
        '''
        anchore bailOnWarn: false, bailOnFail: false, bailOnPluginFail: false, inputQueries: [
          [query: 'cve-scan all'], [query: 'list-packages all'],
          [query: 'list-files all'], [query: 'show-pkg-diffs base']
        ], name: 'images.txt'
      }
    }

    stage('Pushing Docker Image') {
      if(env.REBUILD == 'true' && env.GIT_BRANCH == 'origin/master'){
        sh """
          docker tag fif-terraform:${DOCKERTAG} registry.fif.tech/fif-terraform:${DOCKERTAG}
          docker push registry.fif.tech/fif-terraform:${DOCKERTAG}
          docker tag fif-terraform:${DOCKERTAG} registry.fif.tech/fif-terraform:latest
          docker push registry.fif.tech/fif-terraform:latest
          docker tag fif-terraform:${DOCKERTAG} hub.fif.tech/base/fif-terraform:${DOCKERTAG}
          docker push hub.fif.tech/base/fif-terraform:${DOCKERTAG}
          docker tag fif-terraform:${DOCKERTAG} hub.fif.tech/base/fif-terraform:latest
          docker push hub.fif.tech/base/fif-terraform:latest
          docker rmi fif-terraform:${DOCKERTAG}
          docker rmi registry.fif.tech/fif-terraform:${DOCKERTAG}
          docker rmi registry.fif.tech/fif-terraform:latest
          docker rmi hub.fif.tech/base/fif-terraform:${DOCKERTAG}
          docker rmi hub.fif.tech/base/fif-terraform:latest
        """
        slackSend channel: "#devops", color: "info", message: "New docker image publish: registry.fif.tech/fif-terraform:${DOCKERTAG} and update `latest`."
        slackSend channel: "#devops", color: "info", message: "New docker image publish: hub.fif.tech/base/fif-terraform:${DOCKERTAG} and update `latest`."
      } else {
        print "Skip step. env.REBUILD: '${env.REBUILD}' && env.GIT_BRANCH: '${env.GIT_BRANCH}'"
      }
    }

    stage('Creating new git tag'){
      if(env.REBUILD == 'true' && env.GIT_BRANCH == 'origin/master'){
        sh """
          git tag -f -a ${DOCKERTAG} -m \"version ${DOCKERTAG}\"
          git tag -f -a latest -m \"version ${DOCKERTAG} as latest\"
        """
      } else {
        print "Skip step. env.REBUILD: '${env.REBUILD}' && env.GIT_BRANCH: '${env.GIT_BRANCH}'"
      }
      if(env.GIT_BRANCH == 'origin/master'){
        sshagent(['11f132ab-f4f6-4c0e-9c87-8723dc0e47e7']) {
          sh("git push -f --tags ")
        }
      }
    }

    stage('Notifications') {
      slackSend channel: "#devops_notifications", color: "good", message: "${env.JOB_NAME} ${env.BUILD_DISPLAY_NAME} OK after ${currentBuild.durationString.replace(' and counting', '')} (<${env.BUILD_URL}|Open>)"
      bitbucketStatusNotify ( buildState: 'SUCCESSFUL' )
    }
  }
} catch(error) {
    currentBuild.result = 'FAILURE'
    slackSend channel: "#devops_notifications", color: "danger", message: "${env.JOB_NAME} ${env.BUILD_DISPLAY_NAME} FAILED after ${currentBuild.durationString.replace(' and counting', '')} (<${env.BUILD_URL}|Open>) in stage ${env.STAGE_NAME}."
    bitbucketStatusNotify ( buildState: 'FAILED' )
    throw error
} finally {
  node('azure-slave') {
    stage('Clean Workspace'){
      sh """
        env | sort
        docker rmi fif-terraform:${DOCKERTAG} || true
        docker rmi registry.fif.tech/fif-terraform:${DOCKERTAG} || true
        docker rmi registry.fif.tech/fif-terraform:latest || true
        docker rmi hub.fif.tech/base/fif-terraform:${DOCKERTAG} || true
        docker rmi hub.fif.tech/base/fif-terraform:latest || true
        find ${WORKSPACE} -mindepth 1 -delete
      """
    }
  }
}
