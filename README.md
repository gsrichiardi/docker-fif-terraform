# Docker Image: fif-terraform

This image is child from [`alpine`](https://hub.docker.com/_/alpine/) and is
based on [`terraform:light`](https://github.com/hashicorp/docker-hub-images/blob/master/terraform/Dockerfile-light).

This container include terraform, and those binaries and plugins needed to execute
the different FIFTECH pipelines.

This container is a copy of `hashicorp:light` container. This container will copy
the current stable version of the binary into the container, and set it for use
as the default entrypoint. This will be the best option for most uses, and if
you are just looking to run the binary from a container. The `latest` tag also
points to this version.

You can use this version with the following:
```shell
docker run -i -t registry.fif.tech/fif-terraform:latest <command>
```

For example:
Initialize the plugins (You will only need to do this once)

```shell
docker run -i -t -v $(pwd):/app/ -w /app/ registry.fif.tech/fif-terraform:latest init
```

```shell
docker run -i -t -v $(pwd):/app/ -w /app/ registry.fif.tech/fif-terraform:latest plan
```

## Binary versions



# Changelog
All notable changes to this project will be documented in this section.

## 0.11.11-6 - 2018-01-18
### Added
-   curl command

## 0.11.11-5 - 2018-01-18
### Added
-   Terraform plugin Kong @1.9.1

## 0.11.11-4 - 2018-01-17
### Added
-   envconsul@0.7.3
-   psql from alpine

## 0.11.11-3 - 2018-12-31
### Added
-   Create first image version with terraform@0.11.11 and vault@1.0.1.
